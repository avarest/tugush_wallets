FROM alpine

ENV PROJECT_NAME=wallets

RUN mkdir -p /app
RUN mkdir -p /app/staticfiles
WORKDIR /app

RUN apk update && apk add --no-cache --no-cache libffi-dev libc6-compat py-pip bash python3-dev gcc musl-dev supervisor \
    postgresql-dev nginx redis

ADD requirements/base.txt requirements/base.txt
ADD requirements/prod.txt requirements/prod.txt
RUN pip3 install --upgrade pip && pip3 install --no-cache-dir -r requirements/prod.txt

ADD . /app/

RUN echo "DEBUG=off" >> /app/project/settings/.env
RUN echo "DATABASE_URL=psql://onytrex_chat:xxWrbvp7GD6jEjzvwEXvuLKaT9PAFmCN@tugush.cloctommwngl.eu-central-1.rds.amazonaws.com:5432/wallet_db" >> /app/project/settings/.env
#RUN echo "BTC_WALLETPASS=asdqwe123" >> /app/project/settings/.env
RUN echo "SECRET_KEY=wl4xt0nbvgo0qe9e9s6jsrl!18jc3vg&elu=m5ott6gel(dv*2" >> /app/project/settings/.env
#RUN echo "ETHERSCAN_KEY=KVZGJ2WWUHS7358FCGYPZHDRTHCV92D6YR" >> /app/project/settings/.env
RUN echo "CONTRACT_ADDRESS=0x13be98f0786d372a139c8c387272d2e7030fa296" >> /app/project/settings/.env
#RUN echo "UTXO_URL=http://localhost:3001/insight-api/addr/{0}/utxo" >> /app/project/settings/.env
#RUN echo "ADDR_URL=http://localhost:3001/insight-api/addr/{0}" >> /app/project/settings/.env
RUN echo "BROKER_URL=redis://core.tugush.io:32768/14" >> /app/project/settings/.env
RUN echo "CELERY_RESULT_BACKEND=redis://core.tugush.io:32768/14" >> /app/project/settings/.env
RUN echo "ETH_PROVIDER=http://3.121.26.209:2020/" >> /app/project/settings/.env
RUN echo "BTC_PROVIDER=http://3.121.26.209:2021/" >> /app/project/settings/.env

RUN apk add --update-cache curl bash go && \
    rm -rf /var/cache/apk/* && \
    curl https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-6.5.0-linux-x86_64.tar.gz -o /tmp/filebeat.tar.gz && \
    cd /tmp && \
    tar xzvf filebeat.tar.gz && \
    rm filebeat.tar.gz && \
    mv filebeat-6.5.0-linux-x86_64 /app/filebeat && \
    cd /app/filebeat && \
    cp filebeat /usr/bin && \
    rm -rf filebeat.yml && \
    cp /app/config/filebeat.yml . && \
    ls -ltr filebeat && \
    cat filebeat.yml

#RUN pip install supervisor
RUN pip3 install gunicorn

RUN mkdir -p /run/nginx
RUN echo "daemon off;" >> /etc/nginx/nginx.conf

COPY config/nginx.conf /etc/nginx/conf.d/default.conf

RUN mkdir -p /etc/supervisor.d
COPY config/supervisord.conf /etc/supervisord.conf

RUN python3 manage.py migrate
RUN python3 manage.py collectstatic --noinput

EXPOSE 80

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]
