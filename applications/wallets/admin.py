from django.contrib import admin
from wallets.models import Wallet, Transaction, Currency, TransactionType, TransactionStatus

admin.site.register(Wallet)
admin.site.register(Transaction)
admin.site.register(Currency)
admin.site.register(TransactionType)
admin.site.register(TransactionStatus)
