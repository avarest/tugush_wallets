import uuid
from django.db import models
from django.utils.translation import ugettext_lazy as _
from project.celery import app


class Currency(models.Model):
    name = models.CharField(verbose_name=_('Currency'), max_length=3)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Currency')
        verbose_name_plural = _('Currencies')
        db_table = 'currencies'


class WalletManager(models.Manager):
    def get_queryset(self):
        return super(WalletManager, self).get_queryset().filter(deleted=False)


class Wallet(models.Model):
    address = models.CharField(max_length=255, verbose_name=_('Address'))
    currency = models.ForeignKey('wallets.Currency', on_delete=models.DO_NOTHING, related_name='wallets',
                                 verbose_name=_('Currency'))
    balance = models.DecimalField(verbose_name=_('Balance'), max_digits=30, decimal_places=18, default=0)
    hold = models.DecimalField(verbose_name=_('Hold'), max_digits=30, decimal_places=18, default=0)
    service_code = models.CharField(verbose_name=_('Service'), max_length=2)
    passphrase = models.CharField(max_length=64, verbose_name=_('Passphrase'))
    deleted = models.BooleanField(default=False, verbose_name=_('Deleted'))

    objects = WalletManager()
    all_objects = models.Manager()

    def __str__(self):
        return self.address

    def generate_passphrase(self):
        return str(uuid.uuid4())

    def save(self, *args, **kwargs):
        super(Wallet, self).save(*args, **kwargs)
        if not self.address:
            app.send_task('wallets.tasks.create_address', kwargs={'wallet_id': self.id})

    class Meta:
        verbose_name = _('Wallet')
        verbose_name_plural = _('Wallets')
        unique_together = ('address', 'currency')
        db_table = 'wallets'


class TransactionType(models.Model):
    name = models.CharField(verbose_name=_('Type'), max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Transaction type')
        verbose_name_plural = _('Transaction types')
        db_table = 'transaction_types'


class TransactionStatus(models.Model):
    name = models.CharField(verbose_name=_('Status'), max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Transaction status')
        verbose_name_plural = _('Transaction statuses')
        db_table = 'transaction_statuses'


class Transaction(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=_('Created at'))
    uid = models.CharField(verbose_name=_('Unique transaction id'), max_length=64)
    store = models.IntegerField(verbose_name=_('Store ID'), blank=True, null=True)
    debet_wallet = models.ForeignKey('wallets.Wallet', on_delete=models.DO_NOTHING, verbose_name=_('Debet address'),
                                     blank=True, null=True, related_name='debet_transactions')
    credit_wallet = models.ForeignKey('wallets.Wallet', on_delete=models.DO_NOTHING, verbose_name=_('Credit address'),
                                      max_length=255, blank=True, null=True, related_name='credit_transactions')
    debet_wallet_address = models.CharField(verbose_name=_('Debet address'), max_length=255, blank=True, null=True)
    credit_wallet_address = models.CharField(verbose_name=_('Credit address'), max_length=255, blank=True, null=True)
    amount = models.DecimalField(verbose_name=_('Amount'), max_digits=30, decimal_places=18)
    type = models.ForeignKey('wallets.TransactionType', on_delete=models.DO_NOTHING, related_name='transactions')
    fee = models.DecimalField(verbose_name=_('Fee'), max_digits=30, decimal_places=18, blank=True, null=True)
    details = models.TextField(verbose_name=_('Details'), blank=True, null=True)
    status = models.ForeignKey('wallets.TransactionStatus', on_delete=models.DO_NOTHING, related_name='transactions')
    ext_id = models.CharField(verbose_name=_('External transaction ID'), max_length=255, blank=True, null=True)
    confirmed = models.BooleanField(default=False, verbose_name=_('Confirmed'))
    callback_address = models.CharField(verbose_name=_('Callback Address'), max_length=255, blank=True, null=True)

    def __str__(self):
        return self.uid

    def generate_uid(self):
        return str(uuid.uuid4())

    def save(self, *args, **kwargs):
        if not self.uid:
            self.uid = self.generate_uid()
        super(Transaction, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _('Transaction')
        verbose_name_plural = _('Transactions')
        db_table = 'transactions'
