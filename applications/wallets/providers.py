from bitcoinrpc.authproxy import AuthServiceProxy
from django.conf import settings
from web3 import Web3
import requests
from decimal import Decimal


def value_based_gas_price(amount):
    if amount > Web3.toWei(1, 'ether'):
        return Web3.toWei(6, 'gwei')
    else:
        return Web3.toWei(4.6, 'gwei')


class Eth(object):

    def __init__(self, provider):
        self.w3 = Web3(Web3.HTTPProvider(provider))

    def new_account(self, passphrase):
        return self.w3.personal.newAccount(passphrase)

    def withdrawal(self, sender, recipient, amount, passphrase):
        self.w3.personal.unlockAccount(self.w3.toChecksumAddress(sender), passphrase, 10)
        return self.w3.eth.sendTransaction({'to': self.w3.toChecksumAddress(recipient),
                                            'from': self.w3.toChecksumAddress(sender),
                                            'value': amount,
                                            'gas': 21000,
                                            'gasPrice': value_based_gas_price(amount),
                                            }).hex()


class Bit(object):

    def __init__(self, service_url):
        self.proxy = AuthServiceProxy(service_url)

    def new_account(self):
        return self.proxy.getnewaddress()

    def withdrawal(self, sender, recipient, amount):
        url = settings.UTXO_URL.format(sender)
        r = requests.get(url)
        if r.status_code == 200:
            value = 0
            inputs = []
            for tr in r.json():
                inputs.append({"txid": tr['txid'], "vout": tr['vout']})
                value += tr['amount']

            fee = self.proxy.estimatefee(20)
            rest = Decimal(value)-amount-fee
            if rest >= 0:
                hex = self.proxy.createrawtransaction(inputs, {recipient: amount, sender: rest})
                self.proxy.walletpassphrase(settings.BTC_WALLETPASS, 10)
                signed_hex = self.proxy.signrawtransaction(hex)
                return self.proxy.sendrawtransaction(signed_hex['hex'])
            return {'status': 'Not enough funds for this operation'}


class STQProvider(object):
    tag = 'STQ'
    abi = [
        {"constant": True, "inputs": [],
         "name": "batFundDeposit",
         "outputs": [{"name": "", "type": "address"}],
         "payable": False, "type": "function"},

        {"constant": True, "inputs": [],
         "name": "name",
         "outputs": [{"name": "", "type": "string"}],
         "payable": False, "type": "function"},

        {"constant": False, "inputs": [{"name": "_spender", "type": "address"}, {"name": "_value", "type": "uint256"}],
         "name": "approve", "outputs": [{"name": "success", "type": "bool"}],
         "payable": False, "type": "function"},

        {"constant": True, "inputs": [],
         "name": "totalSupply", "outputs": [{"name": "", "type": "uint256"}],
         "payable": False, "type": "function"},

        {"constant": True, "inputs": [],
         "name": "batFund", "outputs": [{"name": "", "type": "uint256"}],
         "payable": False, "type": "function"},

        {"constant": False, "inputs": [{"name": "_from", "type": "address"}, {"name": "_to", "type": "address"},
                                       {"name": "_value", "type": "uint256"}],
         "name": "transferFrom", "outputs": [{"name": "success", "type": "bool"}],
         "payable": False, "type": "function"},

        {"constant": True, "inputs": [],
         "name": "decimals",
         "outputs": [{"name": "", "type": "uint256"}],
         "payable": False, "type": "function"},

        {"constant": True, "inputs": [],
         "name": "tokenExchangeRate",
         "outputs": [{"name": "", "type": "uint256"}],
         "payable": False, "type": "function"},

        {"constant": False, "inputs": [],
         "name": "finalize", "outputs": [],
         "payable": False,
         "type": "function"},

        {"constant": True, "inputs": [],
         "name": "version", "outputs": [{"name": "", "type": "string"}],
         "payable": False, "type": "function"},

        {"constant": False, "inputs": [],
         "name": "refund", "outputs": [], "payable": False,
         "type": "function"},

        {"constant": True, "inputs": [], "name": "tokenCreationCap",
         "outputs": [{"name": "", "type": "uint256"}],
         "payable": False, "type": "function"},

        {"constant": True, "inputs": [{"name": "_owner", "type": "address"}],
         "name": "balanceOf",
         "outputs": [{"name": "balance", "type": "uint256"}],
         "payable": False, "type": "function"},

        {"constant": True, "inputs": [],
         "name": "isFinalized",
         "outputs": [{"name": "", "type": "bool"}],
         "payable": False, "type": "function"},

        {"constant": True, "inputs": [],
         "name": "fundingEndBlock",
         "outputs": [{"name": "", "type": "uint256"}],
         "payable": False,  "type": "function"},

        {"constant": True, "inputs": [],
         "name": "symbol", "outputs": [{"name": "", "type": "string"}],
         "payable": False, "type": "function"},

        {"constant": True, "inputs": [],
         "name": "ethFundDeposit",
         "outputs": [{"name": "", "type": "address"}],
         "payable": False, "type": "function"},

        {"constant": False,
         "inputs": [{"name": "_to", "type": "address"}, {"name": "_value", "type": "uint256"}],
         "name": "transfer",
         "outputs": [{"name": "success", "type": "bool"}],
         "payable": False, "type": "function"},

        {"constant": False, "inputs": [],
         "name": "createTokens", "outputs": [],
         "payable": True, "type": "function"},

        {"constant": True, "inputs": [],
         "name": "tokenCreationMin", "outputs": [{"name": "", "type": "uint256"}],
         "payable": False, "type": "function"},

        {"constant": True, "inputs": [], "name": "fundingStartBlock",
         "outputs": [{"name": "", "type": "uint256"}],
         "payable": False, "type": "function"},

        {"constant": True, "inputs": [{"name": "_owner", "type": "address"}, {"name": "_spender","type": "address"}],
         "name": "allowance", "outputs": [{"name": "remaining", "type": "uint256"}],
         "payable": False, "type": "function"},
        {"inputs": [{"name": "_ethFundDeposit", "type": "address"},
                    {"name": "_batFundDeposit", "type": "address"},
                    {"name": "_fundingStartBlock", "type": "uint256"},
                    {"name": "_fundingEndBlock", "type": "uint256"}],
         "payable": False, "type": "constructor"},
        {"anonymous": False, "inputs": [{"indexed": True, "name": "_to", "type": "address"},
                                        {"indexed": False, "name": "_value", "type": "uint256"}],
                             "name": "LogRefund", "type": "event"},
        {"anonymous": False, "inputs": [{"indexed": True, "name": "_to", "type": "address"},
                                        {"indexed": False, "name": "_value",
                                         "type": "uint256"}], "name": "CreateBAT",
                                         "type": "event"},
        {"anonymous": False, "inputs": [{"indexed": True, "name": "_from", "type": "address"},
                                        {"indexed": True, "name": "_to", "type": "address"},
                                        {"indexed": False, "name": "_value", "type": "uint256"}],
                             "name": "Transfer", "type": "event"},
        {"anonymous": False, "inputs": [{"indexed": True, "name": "_owner", "type": "address"},
                                        {"indexed": True, "name": "_spender", "type": "address"},
                                        {"indexed": False, "name": "_value", "type": "uint256"}],
                             "name": "Approval", "type": "event"}
    ]
    contract_address = Web3.toChecksumAddress(settings.CONTRACT_ADDRESS)

    def __init__(self, host, etherscan_key, *args, **kwargs):
        self.web3 = Web3(Web3.HTTPProvider(host))
        self.etherscan_key = etherscan_key

    def new_account(self, passphrase):
        if self.web3.isConnected():
            account = self.web3.eth.account.create(passphrase)
            return account.address

    def withdrawal(self, sender, recipient, amount, passphrase):
        contract = self.web3.eth.contract(address=self.contract_address, abi=self.abi)
        self.web3.personal.unlockAccount(self.web3.toChecksumAddress(sender), passphrase, 10)
        contract.functions.transferFrom(self.web3.toChecksumAddress(sender), self.web3.toChecksumAddress(recipient),
                                        amount).call()
