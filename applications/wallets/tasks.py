import logging
import random
from django.conf import settings
from project.celery import app
from project.constants import ETH, BTC
from wallets.models import Wallet
from wallets.providers import Eth, Bit


@app.task(bind=True, max_retries=10)
def create_address(self, wallet_id):
    try:
        wallet = Wallet.objects.get(id=wallet_id)
        if not wallet.passphrase and not wallet.address:
            wallet.passphrase = wallet.generate_passphrase()
            if wallet.currency.name == ETH:
                proxy = Eth(settings.ETH_PROVIDER)
                wallet.address = proxy.new_account(wallet.passphrase)
            elif wallet.currency.name == BTC:
                proxy = Bit(settings.BTC_PROVIDER)
                wallet.address = proxy.new_account()
            wallet.save()
    except Wallet.DoesNotExist as e:
        logging.warning("Tried to create address to non-existing wallet '%s'" % wallet_id)
        self.retry(exc=e, countdown=2 ** self.request.retries)
    except Exception as e:
        logging.warning(e)
        self.retry(exc=e, countdown=int(random.uniform(2, 4) ** self.request.retries))
