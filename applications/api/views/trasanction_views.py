from drf_yasg.utils import swagger_auto_schema
from rest_framework import mixins, viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
from api import parameters
from api.filters.transaction_filters import TransactionFilter
from api.serializers.transaction_serializers import TransactionSerializer, TransactionConfirmSerializer
from wallets.models import Transaction


class TransactionViewSet(mixins.ListModelMixin,
                         mixins.RetrieveModelMixin,
                         viewsets.GenericViewSet):
    serializers = {
        'list': TransactionSerializer,
        'retrieve': TransactionSerializer,
        'metadata': TransactionSerializer,
    }
    filter_class = TransactionFilter
    max_limit = 100
    lookup_field = 'uid'

    def get_serializer_class(self):
        return self.serializers.get(self.action)

    def get_queryset(self):
        return Transaction.objects.all()

    def filter_queryset(self, queryset):
        filter_class = self.filter_class(self.request.GET, queryset, request=self.request)
        queryset = filter_class.qs
        return queryset

    @swagger_auto_schema(method='get', responses={200: parameters.confirm_transaction})
    @action(detail=True, methods=['get'])
    def get_confirm(self, request, pk=None):
        transaction = self.get_object()
        serializer = TransactionConfirmSerializer(instance=transaction)
        return Response(serializer.data, status=status.HTTP_200_OK)
