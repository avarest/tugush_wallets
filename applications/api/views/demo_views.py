from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import api_view
from api import parameters
from api.serializers import demo_serializers


@swagger_auto_schema(method='get', query_serializer=demo_serializers.GetWalletsSerializer,
                     responses={200: parameters.get_wallets})
@api_view(['GET'])
def get_wallets(request):
    """
    Get wallets.
    """
    pass


@swagger_auto_schema(method='get', query_serializer=demo_serializers.GetWalletsCountSerializer,
                     responses={200: parameters.get_wallets_count})
@api_view(['GET'])
def get_wallets_count(request):
    """
    Get wallets count.
    """
    pass


@swagger_auto_schema(method='get', query_serializer=demo_serializers.GetWalletsByIdSerializer,
                     responses={200: parameters.get_wallets_by_id})
@api_view(['GET'])
def get_wallets_by_id(request):
    """
    Get wallets by uids.
    """
    pass


@swagger_auto_schema(method='delete', request_body=demo_serializers.DeleteWalletSerializer,
                     responses={200: parameters.delete_wallet})
@api_view(['DELETE'])
def delete_wallet(request):
    """
    Delete wallet.
    """
    pass


@swagger_auto_schema(method='post', request_body=demo_serializers.AddWalletSerializer,
                     responses={201: parameters.add_wallet})
@api_view(['POST'])
def add_wallet(request):
    """
    Add wallet.
    """
    pass


@swagger_auto_schema(method='get', query_serializer=demo_serializers.GetWalletHoldSerializer,
                     responses={200: parameters.get_hold})
@api_view(['GET'])
def get_hold(request):
    """
    Get hold information.
    """
    pass


@swagger_auto_schema(method='get', query_serializer=demo_serializers.GetWalletHoldSerializer,
                     responses={200: parameters.get_balance})
@api_view(['GET'])
def get_balance(request):
    """
    Get wallet balance.
    """
    pass


@swagger_auto_schema(method='post', request_body=demo_serializers.HoldSerializer,
                     responses={201: parameters.hold_balance})
@api_view(['POST'])
def hold_balance(request):
    """
    Hold funds.
    """
    pass


@swagger_auto_schema(method='post', request_body=demo_serializers.HoldSerializer,
                     responses={201: parameters.unhold_balance})
@api_view(['POST'])
def unhold_balance(request):
    """
    Unhold funds.
    """
    pass


@swagger_auto_schema(method='post', request_body=demo_serializers.DepositSerializer,
                     responses={201: parameters.deposit})
@api_view(['POST'])
def deposit(request):
    """
    Deposit.
    """
    pass


@swagger_auto_schema(method='post', request_body=demo_serializers.WithdrawSerializer,
                     responses={201: parameters.withdraw})
@api_view(['POST'])
def withdraw(request):
    """
    Withdraw.
    """
    pass


@swagger_auto_schema(method='get', query_serializer=demo_serializers.CheckWithdrawSerializer,
                     responses={200: parameters.check_withdraw})
@api_view(['GET'])
def check_withdraw(request):
    """
    Check withdraw.
    """
    pass


@swagger_auto_schema(method='post', request_body=demo_serializers.TransferSerializer,
                     responses={201: parameters.transfer})
@api_view(['POST'])
def transfer(request):
    """
    Transfer.
    """
    pass


@swagger_auto_schema(method='get', query_serializer=demo_serializers.GetTransactionsSerializer,
                     responses={200: parameters.get_transactions})
@api_view(['GET'])
def get_transactions(request):
    """
    Get transactions.
    """
    pass


@swagger_auto_schema(method='get', query_serializer=demo_serializers.GetTransactionsSerializer,
                     responses={200: parameters.get_transactions_count})
@api_view(['GET'])
def get_transactions_count(request):
    """
    Get transactions count.
    """
    pass


@swagger_auto_schema(method='get', query_serializer=demo_serializers.GetTransactionsByIdSerializer,
                     responses={200: parameters.get_transactions_by_id})
@api_view(['GET'])
def get_transactions_by_id(request):
    """
    Get transactions by uids.
    """
    pass


@swagger_auto_schema(method='post', query_serializer=demo_serializers.ConfirmTransactionSerializer,
                     responses={201: parameters.confirm_transaction})
@api_view(['POST'])
def confirm_transaction(request):
    """
    Confirm transaction.
    """
    pass
