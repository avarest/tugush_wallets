from drf_yasg.utils import swagger_auto_schema
from rest_framework import mixins, viewsets, status
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.generics import get_object_or_404
from django.db import transaction
from rest_framework.response import Response
from api.filters.wallets_filters import WalletFilter
from api.serializers.wallets_serializers import WalletSerializer, WalletCreateSerializer, WalletHoldSerializer, \
    WalletDepositSerializer, WalletGetHoldSerializer, WalletGetBalanceSerializer, WalletWithdrawSerializer, \
    WalletCheckWithdrawSerializer, WalletTransferSerializer
from wallets.models import Wallet, Transaction, TransactionType
from api import parameters


class MultipleFieldLookupMixin(object):
    """
    Apply this mixin to any view or viewset to get multiple field filtering
    based on a `lookup_fields` attribute, instead of the default single field filtering.
    """

    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        filter = {}
        for field in self.lookup_fields:
            filter[field] = self.kwargs[field]
        obj = get_object_or_404(queryset, **filter)
        self.check_object_permissions(self.request, obj)
        return obj


class WalletViewSet(MultipleFieldLookupMixin,
                    mixins.CreateModelMixin,
                    mixins.ListModelMixin,
                    mixins.RetrieveModelMixin,
                    mixins.DestroyModelMixin,
                    viewsets.GenericViewSet):
    serializers = {
        'list': WalletSerializer,
        'retrieve': WalletSerializer,
        'create': WalletCreateSerializer,
        'metadata': WalletCreateSerializer,
    }
    filter_class = WalletFilter
    max_limit = 100
    lookup_fields = ('address', 'currency__name')

    def get_serializer_class(self):
        return self.serializers.get(self.action)

    def get_queryset(self):
        return Wallet.objects.all()

    def destroy(self, request, *args, **kwargs):
        wallet = self.get_object()
        if wallet.balance > 0:
            raise ValidationError({"balance": "Wallet cannot be removed with a positive balance."})
        self.perform_destroy(wallet)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def perform_destroy(self, instance):
        instance.deleted = True
        instance.save(update_fields=['deleted'])

    def filter_queryset(self, queryset):
        filter_class = self.filter_class(self.request.GET, queryset, request=self.request)
        queryset = filter_class.qs
        return queryset

    @swagger_auto_schema(method='get', responses={200: parameters.get_hold})
    @action(detail=True, methods=['get'])
    def get_hold(self, request, address=None, currency__name=None):
        wallet = self.get_object()
        serializer = WalletGetHoldSerializer(instance=wallet)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(method='get', responses={200: parameters.get_balance})
    @action(detail=True, methods=['get'])
    def get_balance(self, request, address=None, currency__name=None):
        wallet = self.get_object()
        serializer = WalletGetBalanceSerializer(instance=wallet)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(method='post', request_body=WalletHoldSerializer,
                         responses={201: parameters.hold_balance})
    @action(detail=True, methods=['post'])
    def hold(self, request, address=None, currency__name=None):
        wallet = self.get_object()
        serializer = WalletHoldSerializer(data=request.data)
        if serializer.is_valid():
            amount = serializer.validated_data['amount']
            if wallet.balance < amount:
                raise ValidationError({"amount": "Hold amount exceeds balance"})
            negative_amount = amount * -1
            with transaction.atomic():
                type = TransactionType.objects.get(name='hold')
                tr = Transaction.objects.create(type=type, debet_wallet=wallet, amount=negative_amount)
                wallet.balance += negative_amount
                wallet.hold += amount
                wallet.save()
                return Response({'amount': wallet.hold, 'uid': tr.uid}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(method='post', request_body=WalletHoldSerializer,
                         responses={201: parameters.unhold_balance})
    @action(detail=True, methods=['post'])
    def unhold(self, request, address=None, currency__name=None):
        wallet = self.get_object()
        serializer = WalletHoldSerializer(data=request.data)
        if serializer.is_valid():
            amount = serializer.validated_data['amount']
            if wallet.hold < amount:
                raise ValidationError({"amount": "Unhold amount exceeds hold"})
            negative_amount = amount * -1
            with transaction.atomic():
                type = TransactionType.objects.get(name='unhold')
                tr = Transaction.objects.create(type=type, debet_wallet=wallet, amount=amount)
                wallet.balance += amount
                wallet.hold += negative_amount
                wallet.save()
                return Response({'amount': wallet.hold, 'uid': tr.uid}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(method='post', request_body=WalletDepositSerializer,
                         responses={201: parameters.deposit})
    @action(detail=True, methods=['post'])
    def deposit(self, request, address=None, currency__name=None):
        wallet = self.get_object()
        serializer = WalletDepositSerializer(data=request.data)
        if serializer.is_valid():
            amount = serializer.validated_data['amount']
            with transaction.atomic():
                type = TransactionType.objects.get(name='deposit')
                tr = Transaction.objects.create(type=type, debet_wallet=wallet, **serializer.data)
                wallet.balance += amount
                wallet.save()
                return Response({'amount': wallet.balance, 'uid': tr.uid}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(method='post', request_body=WalletWithdrawSerializer,
                         responses={201: parameters.withdraw})
    @action(detail=True, methods=['post'])
    def withdraw(self, request, address=None, currency__name=None):
        w1 = self.get_object()
        serializer = WalletWithdrawSerializer(data=request.data)
        if serializer.is_valid():
            w2_amount = serializer.validated_data.pop('amount')
            fee = serializer.validated_data.pop('fee')
            amount_to_withdraw = w2_amount + fee
            if w1.balance < amount_to_withdraw:
                raise ValidationError({"balance": "There is not enough money on balance"})
            w1_amount = w2_amount * -1
            address_to = serializer.validated_data.pop('address_to')
            currency_to = serializer.validated_data.pop('currency_to')
            if not w1.currency.name == currency_to:
                raise ValidationError({"currency_to": "Output to wallet only for similar currencies"})
            max_commission = serializer.validated_data.pop('max_commission')
            with transaction.atomic():
                type = TransactionType.objects.get(name='withdraw')
                try:
                    w2 = Wallet.objects.get(address=address_to, currency=currency_to)

                    tr1 = Transaction.objects.create(type=type, debet_wallet=w1, credit_wallet=w2,
                                                     amount=w1_amount, fee=fee, **serializer.data)
                    tr2 = Transaction.objects.create(type=type, debet_wallet=w2, credit_wallet=w1,
                                                     amount=w2_amount, fee=fee, uid=tr1.uid, **serializer.data)
                    w1.balance += amount_to_withdraw * -1
                    w1.save()
                    w2.balance += w2_amount
                    w2.save()
                    return Response({'fee': tr2.fee, 'uid': tr2.uid}, status=status.HTTP_201_CREATED)
                except Wallet.DoesNotExist:
                    tr1 = Transaction.objects.create(type=type, debet_wallet=w1, credit_wallet_address=address_to,
                                                     amount=w1_amount, fee=fee, **serializer.data)
                    tr2 = Transaction.objects.create(type=type, debet_wallet_address=address_to, credit_wallet=w1,
                                                     amount=w2_amount, fee=fee, uid=tr1.uid, **serializer.data)
                    w1.balance += amount_to_withdraw * -1
                    w1.save()
                    # TODO node withdraw
                    return Response({'fee': tr2.fee, 'uid': tr2.uid}, status=status.HTTP_201_CREATED)

        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(method='post', request_body=WalletCheckWithdrawSerializer,
                         responses={200: parameters.check_withdraw})
    @action(detail=True, methods=['post'])
    def check_withdraw(self, request, address=None, currency__name=None):
        wallet = self.get_object()
        serializer = WalletCheckWithdrawSerializer(data=request.data)
        if serializer.is_valid():
            return Response({'fee': 0}, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(method='post', request_body=WalletTransferSerializer,
                         responses={201: parameters.transfer})
    @action(detail=True, methods=['post'])
    def transfer(self, request, address=None, currency__name=None):
        w1 = self.get_object()
        serializer = WalletTransferSerializer(data=request.data)
        if serializer.is_valid():
            w2_amount = serializer.validated_data.pop('amount')
            fee = serializer.validated_data.pop('fee')
            amount_to_withdraw = w2_amount + fee
            currency_to = serializer.validated_data.pop('currency_to')
            if w1.balance < amount_to_withdraw:
                raise ValidationError({"balance": "There is not enough money on balance"})
            if not w1.currency.name == currency_to:
                raise ValidationError({"currency_to": "Output to wallet only for similar currencies"})
            address_to = serializer.validated_data.pop('address_to')
            w1_amount = w2_amount * -1
            with transaction.atomic():
                type = TransactionType.objects.get(name='transfer')
                try:
                    w2 = Wallet.objects.get(address=address_to, currency=currency_to)
                except Wallet.DoesNotExist:
                    raise ValidationError({"address_to": "Wallet does not exists"})
                tr1 = Transaction.objects.create(type=type, debet_wallet=w1, credit_wallet=w2,
                                                 amount=w1_amount, fee=fee, **serializer.data)
                tr2 = Transaction.objects.create(type=type, debet_wallet=w2, credit_wallet=w1,
                                                 amount=w2_amount, fee=fee, uid=tr1.uid, **serializer.data)
                w1.balance += amount_to_withdraw * -1
                w1.save()
                w2.balance += w2_amount
                w2.save()
                return Response({'fee': tr2.fee, 'uid': tr2.uid}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
