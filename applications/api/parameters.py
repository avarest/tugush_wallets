from drf_yasg import openapi
from api.serializers import demo_serializers
from api.serializers.wallets_serializers import WalletGetBalanceSerializer, WalletGetHoldSerializer

get_wallets = openapi.Response('Get wallets', demo_serializers.WalletListSerializer)
get_wallets_count = openapi.Response('Wallets count', demo_serializers.GetWalletsCountResponceSerializer)
get_wallets_by_id = openapi.Response('Get wallets by ids', demo_serializers.WalletListSerializer)
delete_wallet = openapi.Response('Delete wallet', demo_serializers.DeleteWalletResponceSerializer)
add_wallet = openapi.Response('Add wallet', demo_serializers.WalletListSerializer)
get_hold = openapi.Response('Get wallet hold information', WalletGetHoldSerializer)
get_balance = openapi.Response('Get wallet balance', WalletGetBalanceSerializer)
hold_balance = openapi.Response('Hold wallet balance', demo_serializers.HoldResponceSerializer)
unhold_balance = openapi.Response('Unhold wallet balance', demo_serializers.HoldResponceSerializer)
deposit = openapi.Response('Deposit', demo_serializers.DepositResponceSerializer)
withdraw = openapi.Response('Withdraw', demo_serializers.WithdrawResponceSerializer)
check_withdraw = openapi.Response('Check Withdraw', demo_serializers.CheckWithdrawResponceSerializer)
transfer = openapi.Response('Transfer', demo_serializers.TransferResponceSerializer)
get_transactions = openapi.Response('Get transactions', demo_serializers.GetTransactionsResponceSerializer)
get_transactions_count = openapi.Response('Get transactions count', demo_serializers.GetTransactionsCountResponceSerializer)
get_transactions_by_id = openapi.Response('Get transactions by ids', demo_serializers.GetTransactionsResponceSerializer)
confirm_transaction = openapi.Response('Confirm transaction', demo_serializers.ConfirmTransactionResponceSerializer)
