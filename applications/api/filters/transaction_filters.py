import django_filters as filters
from api.filters.wallets_filters import CharInFilter
from wallets.models import Transaction, Wallet


class TransactionFilter(filters.FilterSet):
    created_at = filters.RangeFilter()
    amount = filters.RangeFilter()
    type = CharInFilter(field_name='type__name', lookup_expr='in')
    fee = filters.RangeFilter()
    status = CharInFilter(field_name='status__name', lookup_expr='in')
    debet_wallets = filters.CharFilter(method='filter_debet_wallets')
    credit_wallets = filters.CharFilter(method='filter_credit_wallets')

    def filter_debet_wallets(self, queryset, name, value):
        try:
            lookup = '__'.join(['debet_wallet', 'in'])
            data = eval(value)
            wallets = []
            for wallet in data:
                res = Wallet.objects.filter(address=wallet['address'], currency__name=wallet['currency'])
                if res.exists():
                    wallets.append(res[0])
            return queryset.filter(**{lookup: wallets})
        except:
            return queryset.none()

    def filter_credit_wallets(self, queryset, name, value):
        try:
            lookup = '__'.join(['credit_wallet', 'in'])
            data = eval(value)
            wallets = []
            for wallet in data:
                res = Wallet.objects.filter(address=wallet['address'], currency__name=wallet['currency'])
                if res.exists():
                    wallets.append(res[0])
            return queryset.filter(**{lookup: wallets})
        except:
            return queryset.none()

    class Meta:
        model = Transaction
        fields = ('created_at', 'amount', 'type', 'fee', 'status', 'ext_id', 'confirmed', 'debet_wallets',
                  'credit_wallets')
