import django_filters as filters
from wallets.models import Wallet


class CharInFilter(filters.BaseInFilter, filters.CharFilter):
    pass


class NumberInFilter(filters.BaseInFilter, filters.CharFilter):
    pass


class WalletFilter(filters.FilterSet):
    balance = filters.RangeFilter()
    hold = filters.RangeFilter()
    currencies = CharInFilter(field_name='currency__name', lookup_expr='in')
    service_codes = CharInFilter(field_name='service_code', lookup_expr='in')

    class Meta:
        model = Wallet
        fields = ('address', 'balance', 'hold', 'currencies', 'service_codes')
