from django.conf.urls import url
from api.views import wallets_views, trasanction_views


wallet_list = wallets_views.WalletViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

wallet_detail = wallets_views.WalletViewSet.as_view({
    'get': 'retrieve',
    'delete': 'destroy'
})

wallet_get_hold = wallets_views.WalletViewSet.as_view({
    'get': 'get_hold'
})


wallet_get_balance = wallets_views.WalletViewSet.as_view({
    'get': 'get_balance'
})

wallet_hold = wallets_views.WalletViewSet.as_view({
    'post': 'hold'
})

wallet_unhold = wallets_views.WalletViewSet.as_view({
    'post': 'unhold'
})

wallet_deposit = wallets_views.WalletViewSet.as_view({
    'post': 'deposit'
})

wallet_withdraw = wallets_views.WalletViewSet.as_view({
    'post': 'withdraw'
})

wallet_check_withdraw = wallets_views.WalletViewSet.as_view({
    'post': 'check_withdraw'
})

wallet_transfer = wallets_views.WalletViewSet.as_view({
    'post': 'transfer'
})

transaction_list = trasanction_views.TransactionViewSet.as_view({
    'get': 'list',
})

transaction_detail = trasanction_views.TransactionViewSet.as_view({
    'get': 'retrieve',
})

transaction_confirm = trasanction_views.TransactionViewSet.as_view({
    'get': 'get_confirm',
})

app_name = 'api'

urlpatterns = [
    url(r'^wallets/$', wallet_list, name='wallet-list'),
    url(r'^wallets/(?P<address>[\w_-]+)-(?P<currency__name>[\w_-]+)/$', wallet_detail, name='wallet-detail'),
    url(r'^wallets/(?P<address>[\w_-]+)-(?P<currency__name>[\w_-]+)/get_hold/$', wallet_get_hold,
        name='wallet-get-hold'),
    url(r'^wallets/(?P<address>[\w_-]+)-(?P<currency__name>[\w_-]+)/get_balance/$', wallet_get_balance,
        name='wallet-get-balance'),
    url(r'^wallets/(?P<address>[\w_-]+)-(?P<currency__name>[\w_-]+)/hold/$', wallet_hold, name='wallet-hold'),
    url(r'^wallets/(?P<address>[\w_-]+)-(?P<currency__name>[\w_-]+)/unhold/$', wallet_unhold, name='wallet-unhold'),
    url(r'^wallets/(?P<address>[\w_-]+)-(?P<currency__name>[\w_-]+)/deposit/$', wallet_deposit, name='wallet-deposit'),
    url(r'^wallets/(?P<address>[\w_-]+)-(?P<currency__name>[\w_-]+)/withdraw/$', wallet_withdraw, name='wallet-withdraw'),
    url(r'^wallets/(?P<address>[\w_-]+)-(?P<currency__name>[\w_-]+)/transfer/$', wallet_transfer, name='wallet-transfer'),
    url(r'^wallets/(?P<address>[\w_-]+)-(?P<currency__name>[\w_-]+)/check_withdraw/$', wallet_check_withdraw,
        name='wallet-check_withdraw'),
    url(r'^transactions/$', transaction_list, name='transaction-list'),
    url(r'^transactions/(?P<uid>[\w_-]+)/$', transaction_detail, name='transaction-detail'),
    url(r'^transactions/(?P<uid>[\w_-]+)/get_confirm/$', transaction_confirm, name='transaction-confirm'),
]
