from rest_framework import serializers
from wallets.models import Transaction


class TransactionSerializer(serializers.ModelSerializer):
    debet_wallet = serializers.ReadOnlyField(source='debet_wallet.address')
    debet_wallet_currency = serializers.ReadOnlyField(source='debet_wallet.currency.name')
    credit_wallet = serializers.ReadOnlyField(source='credit_wallet.address')
    credit_wallet_currency = serializers.ReadOnlyField(source='credit_wallet.currency.name')
    type = serializers.ReadOnlyField(source='type.name')
    status = serializers.ReadOnlyField(source='status.name')

    class Meta:
        model = Transaction
        fields = ['uid', 'created_at', 'debet_wallet', 'debet_wallet_currency', 'credit_wallet',
                  'credit_wallet_currency', 'amount', 'type', 'fee', 'details', 'status', 'ext_id', 'confirmed']


class TransactionConfirmSerializer(serializers.ModelSerializer):

    class Meta:
        model = Transaction
        fields = ['confirmed']
