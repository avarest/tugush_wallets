from rest_framework import serializers
from wallets.models import Wallet


class WalletListSerializer(serializers.ModelSerializer):
    result = serializers.CharField()
    currency = serializers.ReadOnlyField(source='currency.name')

    class Meta:
        model = Wallet
        fields = ['result', 'address', 'currency', 'balance', 'hold', 'service_code']


class GetWalletsSerializer(serializers.Serializer):
    address = serializers.CharField(allow_blank=True)
    currencies = serializers.ListField(allow_empty=True)
    balance = serializers.DecimalField(max_digits=30, decimal_places=18, allow_null=True)
    hold = serializers.DecimalField(max_digits=30, decimal_places=18, allow_null=True)
    service_codes = serializers.ListField(allow_empty=True)
    limit = serializers.IntegerField(allow_null=True)
    offset = serializers.IntegerField(allow_null=True)


class GetWalletsCountSerializer(serializers.Serializer):
    address = serializers.CharField(allow_blank=True)
    currencies = serializers.ListField(allow_empty=True)
    balance = serializers.DecimalField(max_digits=30, decimal_places=18, allow_null=True)
    hold = serializers.DecimalField(max_digits=30, decimal_places=18, allow_null=True)
    service_codes = serializers.ListField(allow_empty=True)


class GetWalletsCountResponceSerializer(serializers.Serializer):
    result = serializers.CharField()
    count = serializers.IntegerField()


class GetWalletsByIdSerializer(serializers.Serializer):
    uids = serializers.ListField()


class DeleteWalletSerializer(serializers.Serializer):
    address = serializers.CharField()


class DeleteWalletResponceSerializer(serializers.Serializer):
    result = serializers.CharField()


class AddWalletSerializer(serializers.Serializer):
    currency = serializers.CharField()
    balance = serializers.DecimalField(max_digits=30, decimal_places=18)
    service_codes = serializers.CharField()


class GetWalletHoldSerializer(serializers.Serializer):
    address = serializers.CharField()
    currency = serializers.CharField()


class GetWalletHoldResponceSerializer(serializers.Serializer):
    result = serializers.CharField()
    hold = serializers.DecimalField(max_digits=30, decimal_places=18)


class GetWalletBalanceResponceSerializer(serializers.Serializer):
    result = serializers.CharField()
    balance = serializers.DecimalField(max_digits=30, decimal_places=18)


class HoldSerializer(serializers.Serializer):
    address = serializers.CharField()
    currency = serializers.CharField()
    amount = serializers.DecimalField(max_digits=30, decimal_places=18)


class HoldResponceSerializer(serializers.Serializer):
    amount = serializers.DecimalField(max_digits=30, decimal_places=18)
    uid = serializers.CharField()


class DepositSerializer(serializers.Serializer):
    address = serializers.CharField()
    currency = serializers.CharField()
    amount = serializers.DecimalField(max_digits=30, decimal_places=18)
    callback_address = serializers.CharField()
    store_fee = serializers.DecimalField(max_digits=30, decimal_places=18)
    ext_id = serializers.CharField()


class DepositResponceSerializer(serializers.Serializer):
    amount = serializers.DecimalField(max_digits=30, decimal_places=18)
    uid = serializers.CharField()


class WithdrawSerializer(serializers.Serializer):
    address_from = serializers.CharField()
    address_to = serializers.CharField()
    currency_from = serializers.CharField()
    currency_to = serializers.CharField()
    amount = serializers.DecimalField(max_digits=30, decimal_places=18)
    max_fee = serializers.DecimalField(max_digits=30, decimal_places=18)
    store_fee = serializers.DecimalField(max_digits=30, decimal_places=18)
    callback_address = serializers.CharField()
    ext_id = serializers.CharField()


class WithdrawResponceSerializer(serializers.Serializer):
    fee = serializers.DecimalField(max_digits=30, decimal_places=18)
    uid = serializers.CharField()


class CheckWithdrawSerializer(serializers.Serializer):
    address = serializers.CharField()
    currency = serializers.CharField()
    amount = serializers.DecimalField(max_digits=30, decimal_places=18)


class CheckWithdrawResponceSerializer(serializers.Serializer):
    fee = serializers.DecimalField(max_digits=30, decimal_places=18)


class TransferSerializer(serializers.Serializer):
    address_from = serializers.CharField()
    address_to = serializers.CharField()
    currency_from = serializers.CharField()
    currency_to = serializers.CharField()
    amount = serializers.DecimalField(max_digits=30, decimal_places=18)
    store_fee = serializers.DecimalField(max_digits=30, decimal_places=18)
    ext_id = serializers.CharField()


class TransferResponceSerializer(serializers.Serializer):
    fee = serializers.DecimalField(max_digits=30, decimal_places=18)
    uid = serializers.CharField()


class GetTransactionsSerializer(serializers.Serializer):
    create = serializers.DateTimeField()
    amount = serializers.DecimalField(max_digits=30, decimal_places=18, allow_null=True)
    type = serializers.ListField()
    status = serializers.ListField()
    fee = serializers.DecimalField(max_digits=30, decimal_places=18, allow_null=True)
    limit = serializers.IntegerField(allow_null=True)
    offset = serializers.IntegerField(allow_null=True)
    ext_id = serializers.CharField(allow_blank=True)
    confirmed = serializers.BooleanField()
    addresses = serializers.ListField(allow_empty=True)
    currency = serializers.CharField()


class GetTransactionsResponceSerializer(serializers.Serializer):
    result = serializers.CharField()
    uid = serializers.CharField()
    created_at = serializers.DateTimeField()
    address_debet = serializers.CharField()
    address_credit = serializers.CharField()
    currency_debet = serializers.CharField()
    currency_credit = serializers.CharField()
    amount = serializers.DecimalField(max_digits=30, decimal_places=18)
    fee = serializers.DecimalField(max_digits=30, decimal_places=18)
    type = serializers.CharField()
    details = serializers.JSONField()
    status = serializers.CharField()
    ext_id = serializers.CharField()
    confirmed = serializers.BooleanField()


class GetTransactionsCountResponceSerializer(serializers.Serializer):
    result = serializers.CharField()
    count = serializers.IntegerField()


class GetTransactionsByIdSerializer(serializers.Serializer):
    uids = serializers.ListField()


class ConfirmTransactionSerializer(serializers.Serializer):
    uid = serializers.CharField()


class ConfirmTransactionResponceSerializer(serializers.Serializer):
    confirmed = serializers.BooleanField()
