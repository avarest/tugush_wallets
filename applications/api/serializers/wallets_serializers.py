from rest_framework import serializers
from wallets.models import Wallet, Currency


class WalletSerializer(serializers.ModelSerializer):
    currency = serializers.ReadOnlyField(source='currency.name')

    class Meta:
        model = Wallet
        fields = ['address', 'currency', 'balance', 'hold', 'service_code']


class WalletCreateSerializer(serializers.ModelSerializer):
    currency = serializers.CharField()

    def validate_currency(self, obj):
        try:
            currency = Currency.objects.get(name=obj.upper())
            return currency
        except Currency.DoesNotExist:
            raise serializers.ValidationError("Currency not found")

    def create(self, validated_data):
        currency = validated_data.pop('currency')
        user = Wallet.objects.create(**validated_data, currency=currency)
        return user

    class Meta:
        model = Wallet
        fields = ['currency', 'balance', 'service_code']


class WalletHoldSerializer(serializers.Serializer):
    amount = serializers.DecimalField(max_digits=30, decimal_places=18)

    def validate_amount(self, obj):
        if obj <= 0:
            raise serializers.ValidationError({"amount": "The amount was not positive"})
        return obj


class WalletDepositSerializer(serializers.Serializer):
    amount = serializers.DecimalField(max_digits=30, decimal_places=18)
    callback_address = serializers.CharField(allow_blank=True, allow_null=True, default=None)
    fee = serializers.DecimalField(max_digits=30, decimal_places=18, default=0)
    ext_id = serializers.CharField(allow_blank=True, allow_null=True, default=None)

    def validate_amount(self, obj):
        if obj <= 0:
            raise serializers.ValidationError({"amount": "The amount was not positive"})
        return obj

    def validate_fee(self, obj):
        if obj <= 0:
            raise serializers.ValidationError({"fee": "The amount was not positive"})
        return obj


class WalletGetHoldSerializer(serializers.ModelSerializer):

    class Meta:
        model = Wallet
        fields = ['hold']


class WalletGetBalanceSerializer(serializers.ModelSerializer):

    class Meta:
        model = Wallet
        fields = ['balance']


class WalletWithdrawSerializer(serializers.Serializer):
    amount = serializers.DecimalField(max_digits=30, decimal_places=18)
    address_to = serializers.CharField()
    currency_to = serializers.CharField()
    max_commission = serializers.DecimalField(max_digits=30, decimal_places=18, default=0)
    fee = serializers.DecimalField(max_digits=30, decimal_places=18, default=0)
    callback_address = serializers.CharField(allow_blank=True, allow_null=True, default=None)
    ext_id = serializers.CharField(allow_blank=True, allow_null=True, default=None)

    def validate_amount(self, obj):
        if obj <= 0:
            raise serializers.ValidationError({"amount": "The amount was not positive"})
        return obj

    def validate_fee(self, obj):
        if obj <= 0:
            raise serializers.ValidationError({"fee": "The amount was not positive"})
        return obj

    def validate_max_commission(self, obj):
        if obj <= 0:
            raise serializers.ValidationError({"max_commission": "The amount was not positive"})
        return obj


class WalletCheckWithdrawSerializer(serializers.Serializer):
    amount = serializers.DecimalField(max_digits=30, decimal_places=18)


class WalletTransferSerializer(serializers.Serializer):
    amount = serializers.DecimalField(max_digits=30, decimal_places=18)
    address_to = serializers.CharField()
    currency_to = serializers.CharField()
    fee = serializers.DecimalField(max_digits=30, decimal_places=18, default=0)
    ext_id = serializers.CharField(allow_blank=True, allow_null=True, default=None)

    def validate_amount(self, obj):
        if obj <= 0:
            raise serializers.ValidationError({"amount": "The amount was not positive"})
        return obj

    def validate_fee(self, obj):
        if obj <= 0:
            raise serializers.ValidationError({"fee": "The amount was not positive"})
        return obj
